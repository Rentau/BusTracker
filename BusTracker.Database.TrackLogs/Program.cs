﻿namespace BusTracker.Database.TrackLogs
{
    using System;
    using System.IO;
    using System.Reflection;
    using DbUp;
    using Microsoft.Extensions.Configuration;

    public class Program
    {
        public static int Main()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("DatabaseSettings.json", false, true)
                .AddJsonFile("DatabaseSettings.Development.json", optional: true)
                .Build();

            var connectionString = config.GetConnectionString("TrackLogsDatabase")
                                   ?? "Server=(localdb)\\MSSQLLocalDB; Database=TrackLogs; Trusted_connection=true";

            EnsureDatabase.For.SqlDatabase(connectionString);

            var upgradeEngine =
                DeployChanges.To
                    .SqlDatabase(connectionString)
                    .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                    .LogToConsole()
                    .Build();

            var result = upgradeEngine.PerformUpgrade();

            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
                return -1;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();
            return 0;
        }
    }
}
