IF NOT EXISTS (
		SELECT 1
		FROM [sys].[schemas] s
		WHERE s.[name] = 'tracking'
		)
BEGIN
	PRINT 'Creating Schema Tracking'

	EXEC ('CREATE SCHEMA tracking')
END