IF NOT EXISTS (
	SELECT 1
	FROM [sys].[objects] o
	WHERE o.[object_id] = OBJECT_ID(N'[tracking].[dozortracklog]')
		AND o.[type] = N'U'
	)
BEGIN

PRINT 'Creating Table [tracking].[dozortracklog]'

CREATE TABLE [tracking].[dozortracklog](
	[imei]				VARCHAR (15)			NOT NULL
	,[time]				DATETIME2 (0)			NOT NULL
	,[lat]				DECIMAL(9,6)			NOT NULL
	,[lng]				DECIMAL(9,6)			NOT NULL
	,[spd]				TINYINT					NOT NULL
	,[sat]				TINYINT					NOT NULL
)

END

GO