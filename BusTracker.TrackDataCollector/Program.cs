﻿namespace BusTracker.TrackDataCollector
{
    using System.Diagnostics;
    using System.IO;
    using BusTracker.TrackDataCollector.DataProviders;
    using BusTracker.TrackDataCollector.DataProviders.Dozor;
    using BusTracker.Utilities.Extensions;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Serilog;

    public class Program
    {
        public static void Main()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("DatabaseSettings.json", false, true)
                .AddJsonFile("LoggerSettings.json", false, true);

            if (Debugger.IsAttached)
            {
                configBuilder.AddJsonFile("DatabaseSettings.Development.json", optional: true, true);
                configBuilder.AddJsonFile("LoggerSettings.Development.json", optional: true, true);
            }

            var config = configBuilder.Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();

            var serviceProvider = new ServiceCollection()
                .AddLogging(loggingBuilder =>
                {
                    loggingBuilder.AddSerilog();
                })
                .AddSingleton<TrackDataCollector, TrackDataCollector>()
                .AddSingleton<IDataUpdater, DozorDataUpdater>()
                .AddSingleton<DozorTrackDataRepository, DozorTrackDataRepository>()
                .AddSingleton<DozorDataProvider, DozorDataProvider>()
                .AddSingleton<IConfiguration>(config)
                .BuildServiceProvider();

            var collector = serviceProvider.GetService<TrackDataCollector>();
            UpdateLoop.RunUntilEscape(() => collector.CollectUpdates(), 1000);

            Log.CloseAndFlush();
        }
    }
}
