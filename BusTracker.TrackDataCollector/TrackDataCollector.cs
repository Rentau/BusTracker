﻿namespace BusTracker.TrackDataCollector
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BusTracker.TrackDataCollector.DataProviders;
    using Microsoft.Extensions.Logging;

    public class TrackDataCollector
    {
        private readonly List<IDataUpdater> trackers;
        private readonly ILogger<TrackDataCollector> logger;

        public TrackDataCollector(ILogger<TrackDataCollector> logger, IEnumerable<IDataUpdater> trackers)
        {
            this.logger = logger;
            this.trackers = trackers.ToList();
        }

        public void CollectUpdates()
        {
            foreach (var tracker in this.trackers)
            {
                try
                {
                    tracker.Update();
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex.GetBaseException(), "Collecting Updates failed!");
                }
            }
        }
    }
}