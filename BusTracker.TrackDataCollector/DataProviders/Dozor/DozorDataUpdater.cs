﻿namespace BusTracker.TrackDataCollector.DataProviders.Dozor
{
    using System.Linq;
    using Microsoft.Extensions.Logging;

    public class DozorDataUpdater : IDataUpdater
    {
        private readonly DozorDataProvider provider;
        private readonly DozorTrackDataRepository repository;
        private readonly ILogger<DozorDataUpdater> logger;

        public DozorDataUpdater(DozorDataProvider provider, DozorTrackDataRepository repository, ILogger<DozorDataUpdater> logger)
        {
            this.provider = provider;
            this.repository = repository;
            this.logger = logger;
        }

        public void Update()
        {
            var updates = this.provider.GetTrackingUpdatesAsync().Result.ToList();

            if (updates.Any())
            {
                this.logger.LogInformation($"Received {updates.Count} updates");
                this.repository.AddTrackData(updates);
            }
        }
    }
}