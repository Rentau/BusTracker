﻿namespace BusTracker.TrackDataCollector.DataProviders.Dozor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using BusTracker.TrackDataCollector.DataProviders.Dozor.Entities;
    using Microsoft.Extensions.Logging;

    public class DozorDataProvider
    {
        private readonly ILogger<DozorDataProvider> logger;
        private const int ServerRefreshSeconds = 10;
        private readonly DozorDataLoader dataLoader;
        private DateTime? lastUpdated;
        private static readonly SemaphoreSlim SlowStuffSemaphore = new SemaphoreSlim(1, 1);

        public DozorDataProvider(ILogger<DozorDataProvider> logger)
        {
            this.logger = logger;
            this.dataLoader = new DozorDataLoader();
        }

        public async Task<IEnumerable<DozorTrackDataModel>> GetTrackingUpdatesAsync()
        {
            await SlowStuffSemaphore.WaitAsync();

            var currentTime = DateTime.UtcNow;
            var lastUpdatedTime = this.lastUpdated ?? currentTime - TimeSpan.FromSeconds(ServerRefreshSeconds * 2);
            var timeDiff = currentTime - lastUpdatedTime;
            var shouldLoadUpdate = timeDiff > TimeSpan.FromSeconds(ServerRefreshSeconds);

            try
            {
                if (shouldLoadUpdate)
                {
                    return await this.LoadUpdates(lastUpdatedTime);
                }
            }
            catch (Exception e) 
            {
                this.logger.LogError(e, "Can't load data.");
            }
            finally
            {
                SlowStuffSemaphore.Release();
            }

            return Enumerable.Empty<DozorTrackDataModel>();
        }

        private async Task<IEnumerable<DozorTrackDataModel>> LoadUpdates(DateTime lastUpdatedTime)
        {
            var data = await this.dataLoader.GetTrackData();
            var trackers = data.OrderBy(_ => _.Time).Select(t => new DozorTrackDataModel(t)).ToList();
            this.lastUpdated = trackers.Max(_ => _.TimeStamp);
            return trackers.Where(_ => _.TimeStamp > lastUpdatedTime);
        }
    }
}
