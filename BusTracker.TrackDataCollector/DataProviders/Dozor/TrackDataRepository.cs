﻿namespace BusTracker.TrackDataCollector.DataProviders.Dozor
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using BusTracker.TrackDataCollector.DataProviders.Dozor.Entities;
    using Dapper;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;

    public class DozorTrackDataRepository
    {
        private readonly IConfiguration config;
        private readonly ILogger<DozorTrackDataRepository> logger;

        public DozorTrackDataRepository(IConfiguration config, ILogger<DozorTrackDataRepository> logger)
        {
            this.config = config;
            this.logger = logger;
        }

        public IDbConnection Connection => new SqlConnection(this.config.GetConnectionString("TrackLogsDatabase"));

        public void AddTrackData(IEnumerable<DozorTrackDataModel> data)
        {
            using (var connection = this.Connection)
            {
                connection.Open();
                var transaction = connection.BeginTransaction();

                try
                {
                    connection.Execute(
                        @"insert [tracking].[dozortracklog] ([imei], [time], [lng], [lat], [sat], [spd]) values (@Imei, @TimeStamp, @Longitude, @Latitude, @Satellite, @Speed)",
                        data,
                        transaction: transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, "Can't insert Dozor tracks data into TrackLogsDatabase");
                    transaction.Rollback();
                }
            }
        }
    }
}