﻿namespace BusTracker.TrackDataCollector.DataProviders.Dozor
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BusTracker.TrackDataCollector.DataProviders.Dozor.Entities.DTO;
    using BusTracker.Utilities;
    using BusTracker.Utilities.Serializers;

    public class DozorDataLoader
    {
        private readonly HttpRequestHandler requestHandler;
        private readonly JsonSerializer jsonSerializer;

        public DozorDataLoader()
        {
            var httpClient = new HttpClient
            {
                Timeout = TimeSpan.FromSeconds(10),
                DefaultRequestHeaders = { { "Accept-Encoding", "gzip, deflate, br" } }
            };

            this.requestHandler = new HttpRequestHandler(httpClient);
            this.jsonSerializer = new JsonSerializer();
        }

        public async Task<List<TrackerData>> GetTrackData()
        {
            const string requestPath = "http://t.dozor-gps.com.ua/data?t=17";
            return await this.ProcessRequest<List<TrackerData>>(requestPath);
        }

        private async Task<TEntity> ProcessRequest<TEntity>(string requestPath)
            where TEntity: class, new()
        {
            var content = await this.requestHandler.HandleAsync(requestPath);
            return this.jsonSerializer.Deserialize<TEntity>(content);
        }
    }
}
