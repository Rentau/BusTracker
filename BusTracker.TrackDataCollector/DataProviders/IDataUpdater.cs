﻿namespace BusTracker.TrackDataCollector.DataProviders
{
    public interface IDataUpdater
    {
        void Update();
    }
}