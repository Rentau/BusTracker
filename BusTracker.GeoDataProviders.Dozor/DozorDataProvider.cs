﻿namespace BusTracker.GeoDataProviders.Dozor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BusTracker.Contracts.Interfaces;
    using BusTracker.GeoDataProviders.Dozor.Entities;

    public class DozorDataProvider : ITrackingInfoProvider
    {
        private const int ServerRefreshSeconds = 10;
        private readonly DataLoader dataLoader;
        private DateTime? lastUpdated;

        public DozorDataProvider()
        {
            this.dataLoader = new DataLoader();
        }

        public IEnumerable<IVehicleTracker> GetTrackingUpdates()
        {
            IEnumerable<IVehicleTracker> updates = new List<VehicleTracker>();
            
            var currentTime = DateTime.UtcNow;
            var lastUpdatedTime = this.lastUpdated ?? currentTime - TimeSpan.FromSeconds(ServerRefreshSeconds * 2);
            var timeDiff = currentTime - lastUpdatedTime;
            var isOutdated = timeDiff > TimeSpan.FromSeconds(ServerRefreshSeconds);

            if (isOutdated)
            {
                var data = this.dataLoader.GetFreeData().Result;
                var trackers = data.OrderBy(_ => _.Time).Select(t => new VehicleTracker(t)).ToList();
                
                var lastUpdatedEvent = trackers.Max(_ => _.TimeStamp);
                updates = trackers.Where(x => x.TimeStamp > lastUpdatedTime);
                this.lastUpdated = lastUpdatedEvent;
            }

            return updates;
        }
    }
}
