﻿namespace BusTracker.Core.Events
{
    using System.Collections.Generic;
    using BusTracker.Contracts.Interfaces;

    public class TrackingUpdatesEvent : IEvent
    {
        public List<IVehicleTracker> TrackingData { get;  }

        public TrackingUpdatesEvent(List<IVehicleTracker> trackingData)
        {
            this.TrackingData = trackingData;
        }
    }
}