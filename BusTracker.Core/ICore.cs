﻿namespace BusTracker.Core
{
    public interface ICore
    {
        void Start();
        void Update();
    }
}