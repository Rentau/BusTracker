﻿namespace BusTracker.Core
{
    using System.Collections.Generic;
    using System.Linq;
    using BusTracker.Contracts.Interfaces;
    using BusTracker.Core.Events;
    using BusTracker.GeoDataProviders.Dozor;
    using BusTracker.Infrastructure.CQRS.Commands;
    using BusTracker.Infrastructure.CQRS.Dispatchers;
    using BusTracker.Infrastructure.EventHandling;
    using Microsoft.Extensions.Logging;

    public class Core : ICore
    {
        private readonly List<ITrackingInfoProvider> providers;
        private readonly IEventAggregator eventAggregator;
        private readonly ILogger<Core> logger;
        private readonly ICommandDispatcher commandDispatcher;

        public Core(IEventAggregator eventAggregator, ILogger<Core> logger, ICommandDispatcher commandDispatcher)
        {
            this.providers = new List<ITrackingInfoProvider>
            {
                new DozorDataProvider()
            };

            this.eventAggregator = eventAggregator;
            this.logger = logger;
            this.commandDispatcher = commandDispatcher;
        }

        public void Start()
        {
            this.eventAggregator.Subscribe<TrackingUpdatesEvent>(this.StoreEvents);
        }

        private void StoreEvents(TrackingUpdatesEvent trackingData)
        {
            foreach (var currentData in trackingData.TrackingData)
            {
                this.commandDispatcher.Execute(new AddTrackingDataCommand(currentData));
            }
        }

        public void Update()
        {
            foreach (var currentProvider in this.providers)
            {
                var data = currentProvider.GetTrackingUpdates().ToList();
                var type = currentProvider.GetType();
                this.logger.LogInformation($"Updates [{type.Name}]: \n{ string.Join("\n", data.Select(_ => $"{_.Imei}, \t {_.Speed}, \t {_.TimeStamp}"))}");
                this.eventAggregator.Publish(new TrackingUpdatesEvent(data));
            }
        }

        //private static IStationInformation GetClosestStation(GeoCoordinate vehicleLocation, IEnumerable<IStationInformation> trackingData)
        //{
        //    var closestStation = trackingData.OrderBy(x => vehicleLocation.GetDistanceTo(x.Location)).FirstOrDefault();
        //    return closestStation;
        //}
    }
}
