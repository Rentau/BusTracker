﻿namespace BusTracker.Analyzer.Console
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Newtonsoft.Json;
    using GeoCoordinatePortable;
    using BusTracker.Analyzer.Console.Repositories;

    public class Core
    {
        private readonly ITrackDataRepository trackDataRepository;
        
        public Core(ITrackDataRepository trackDataRepository)
        {
            this.trackDataRepository = trackDataRepository;
        }

        public void Analyze()
        {
            var data = this.trackDataRepository.GetTrackDataByDate(DateTime.Parse("2019-07-31"));
            var groupedData = data.GroupBy(_ => _.Imei).ToDictionary(_ => _.Key, _ => _.ToList());
            //var mostAvailable = groupedData.Aggregate((l, r) => l.Value.Count > r.Value.Count ? l : r).Key;

            var allRoutesEndPoint = new List<StopInfo>();

            var validDates = new List<DateTime>
            {
                new DateTime(2019, 08, 01),
                new DateTime(2019, 07, 31),
                new DateTime(2019, 07, 30),
                new DateTime(2019, 07, 25),
                new DateTime(2019, 07, 24),
            };

            var selected = groupedData.Keys;
            //var selected = new[] {"353173060777321"};

            foreach (var imei in selected)
            {
                var allDataByImei = this.trackDataRepository.GetTrackDataByImei(imei).Where(_ => validDates.Contains(_.TimeStamp.Date)).ToList();
                //var zeroSpeedTracks = KMeansStatic.Process(allDataByImei.Where(_ => _.Speed == 0), 300).ToList();
                //var zeroSpeedPositions = zeroSpeedTracks.Select(_ => new GeoCoordinate(_.Latitude, _.Longitude)).ToList();

                //var uniqueZeroSpeedPositions = RemoveDuplicates(zeroSpeedPositions).ToList();

                //File.WriteAllText("StopPoints.geojson", JsonConvert.SerializeObject(GeoJsonHelper.CreateFeature(uniqueZeroSpeedPositions, null)));
                
                //var trackList = allDataByImei.Select(_ => new GeoCoordinate(_.Latitude, _.Longitude)).ToList();

                var tracksByDate = allDataByImei.ToLookup(_ => _.TimeStamp.Date).ToDictionary(_ => _.Key, _ => _.ToArray());

                foreach (var key in tracksByDate.Keys)
                {
                    var allTracksByDay = tracksByDate[key].ToList();

                    if (allTracksByDay.Count < 1000)
                    {
                        continue;
                    }

                    var filteredTracksByDay = TrimTracks(allTracksByDay).ToList();
                    var sleepPoints = allTracksByDay.Except(filteredTracksByDay).ToList();

                    if (!sleepPoints.Any())
                    {
                        continue;
                    }

                    if (filteredTracksByDay.Count < 600 || filteredTracksByDay.Count() < filteredTracksByDay.Count(_ => _.Speed < 10) * 3)
                    {
                        continue;
                    }

                    var sleepPoint = GetDurationAtPoint(sleepPoints).OrderByDescending(_ => _.Value.Count).First().Key;


                    var zeroSpeedTracksByDay = KMeansStatic.Process(filteredTracksByDay.Where(_ => _.Speed < 10), 300).ToList();
                    var zeroSpeedPositionsByDay = zeroSpeedTracksByDay.Select(_ => new GeoCoordinate(_.Latitude, _.Longitude)).ToList();
                    var uniqueZeroSpeedPositionsByDay = RemoveDuplicates(zeroSpeedPositionsByDay).ToList();

                    var filteredCoordinates = filteredTracksByDay.Select(_ => new GeoCoordinate(_.Latitude, _.Longitude)).ToList();
                    var groupedByFrequency = AnalyzeFrequency(uniqueZeroSpeedPositionsByDay, filteredCoordinates);
                    var features = GeoJsonHelper.GetFeatures(groupedByFrequency);
                    File.WriteAllText($"Frequency-{key:dd-MM-yyyy}.geojson", JsonConvert.SerializeObject(features));
                    File.WriteAllText($"StopPoints-{key:dd-MM-yyyy}.geojson", JsonConvert.SerializeObject(GeoJsonHelper.CreateFeature(uniqueZeroSpeedPositionsByDay, null)));

                    //var mostDistantPoints = GetMostDistantPoints(uniqueZeroSpeedPositionsByDay);
                    //File.WriteAllText($"MostDistant-{key:dd-MM-yyyy}.geojson", JsonConvert.SerializeObject(GeoJsonHelper.CreateFeature(new [] { mostDistantPoints.Item1, mostDistantPoints.Item2}, null)));

                    //var mostVisitedPoint = GetMostVisitedPoint(zeroSpeedPositionsByDay);
                    //var filteredTracksByDay2 = RemoveTimeDuplicates(filteredTracksByDay).ToList();
                    //var times = GetCountAtPoint(filteredTracksByDay2, mostVisitedPoint).ToList();
                    var longestStops = GetDurationAtPoint(filteredTracksByDay);
                    var longestStopWithoutSleep = longestStops
                        .Where(_ => FromModel(_.Key).GetDistanceTo(FromModel(sleepPoint)) > 300)
                        .OrderByDescending(_ => _.Value.Count).ToList();

                    if (longestStopWithoutSleep.Any())
                    {
                        KeyValuePair<RawDataModel, List<RawDataModel>>? selectedPoint = null;
                        foreach (var longStop in longestStopWithoutSleep)
                        {
                            var pointMatches = filteredTracksByDay.Where(_ =>
                                FromModel(_).GetDistanceTo(FromModel(longStop.Key)) < 100);

                            var uniquePointMatches = RemoveTimeDuplicates(pointMatches);

                            if (uniquePointMatches.Count() > 10)
                            {
                                selectedPoint = longStop;
                                break;
                            }
                        }

                        if (!selectedPoint.HasValue)
                        {
                            continue;
                        }

                        var routeEndPoint = selectedPoint.Value.Key;
                        var stopsData = selectedPoint.Value.Value;
                        allRoutesEndPoint.Add(new StopInfo
                        {
                            Point = routeEndPoint,
                            FromTime = stopsData.First().TimeStamp,
                            ToTime = stopsData.Last().TimeStamp,
                            Count = stopsData.Count
                        });
                    }
                }

            // var groupedByFrequency = AnalyzeFrequency(uniqueZeroSpeedPositions, trackList);
            //             var features = GetFeatures(groupedByFrequency);
            // var actualJson = JsonConvert.SerializeObject(features);
            // File.WriteAllText("Frequency.geojson", actualJson);
            }

            File.WriteAllText($"EndRoutes.geojson", JsonConvert.SerializeObject(GeoJsonHelper.CreateFeatures(allRoutesEndPoint)));
        }

        public class StopInfo
        {
            public RawDataModel Point { get; set; }

            public DateTime FromTime { get; set; }

            public DateTime ToTime { get; set; }

            public int Count { get; set; }
        }

        private static Dictionary<RawDataModel, List<RawDataModel>> GetDurationAtPoint(IReadOnlyCollection<RawDataModel> tracks)
        {
            var stopTracks = tracks.Where(_ => _.Speed < 10);
            var result = new Dictionary<RawDataModel, List<RawDataModel>>();
            foreach (var current in stopTracks)
            {
                var trackInRow = tracks.SkipWhile(_ => _ != current)
                    .Skip(1)
                    .TakeWhile(_ => FromModel(_).GetDistanceTo(FromModel(current)) < 50).ToList();
                result[current] = trackInRow;
            }

            var data = result.OrderByDescending(_ => _.Value.Count).ToList();
            return result;
        }

        private static GeoCoordinate FromModel(RawDataModel rawTrack)
        {
            return new GeoCoordinate(rawTrack.Latitude, rawTrack.Longitude);
        }

        private static IEnumerable<RawDataModel> TrimTracks(List<RawDataModel> allTracksByDay)
        {
            int speedInTheRow = 0;
            int firstIndex = -1;
            int lastIndex = -1;

            int fromIndex = 0;
            int toIndex = allTracksByDay.Count - 1;

            for (var i = 0; i < allTracksByDay.Count; i++)
            {
                var current = allTracksByDay[i];

                if (current.Speed > 0)
                {
                    if (speedInTheRow > 0)
                    {
                        lastIndex = i;
                    }
                    else
                    {
                        firstIndex = i;
                    }

                    speedInTheRow++;
                }
                else
                {
                    if (speedInTheRow > 0)
                    {
                        speedInTheRow = 0;
                    }
                }

                if (speedInTheRow > 5)
                {
                    if (fromIndex == 0)
                    {
                        fromIndex = firstIndex;
                    }

                    toIndex = lastIndex + 1;
                }
            }

            //Console.WriteLine($"Cleared data before: \n {string.Join("\n ", allTracksByDay.GetRange(0, fromIndex).Select(_ => $"{_.TimeStamp:HH:mm:ss} {_.Speed}"))}");
            //Console.WriteLine($"First 10: \n {string.Join("\n ", allTracksByDay.GetRange(fromIndex, 10).Select(_ => $"{_.TimeStamp:HH:mm:ss} {_.Speed}"))}");
            //Console.WriteLine($"Last 10 : \n {string.Join("\n ", allTracksByDay.GetRange(toIndex - 10, 10).Select(_ => $"{_.TimeStamp:HH:mm:ss} {_.Speed}"))}");
            //Console.WriteLine($"Cleared data after : \n {string.Join("\n ", allTracksByDay.GetRange(toIndex, allTracksByDay.Count - toIndex).Select(_ => $"{_.TimeStamp:HH:mm:ss} {_.Speed}"))}");
            var result = allTracksByDay.GetRange(fromIndex, toIndex - fromIndex);
            //Console.WriteLine($"{result.First().TimeStamp:HH:mm:ss} - {result.Last().TimeStamp:HH:mm:ss}");
            return result;
        }

        private static Dictionary<GeoCoordinate, int> AnalyzeFrequency(IEnumerable<GeoCoordinate> pointOfInterest, IReadOnlyList<GeoCoordinate> trackList)
        {
            var resultData = new Dictionary<GeoCoordinate, int>();
            foreach (var currentPoint in pointOfInterest)
            {
                var count = trackList.Count(_ => _.GetDistanceTo(currentPoint) < 50);
                resultData[currentPoint] = count;
                //Console.WriteLine($"{currentPoint.Latitude}, {currentPoint.Longitude} = {count}");
            }

            return resultData;
        }

        internal class Wrapper<T>
        {
            internal Wrapper(T coordinate)
            {
                this.Coordinate = coordinate;
                this.IsDeleted = false;
            }

            internal T Coordinate;

            internal bool IsDeleted;
        }

        private static IEnumerable<GeoCoordinate> RemoveDuplicates(IEnumerable<GeoCoordinate> zeroSpeedPositions)
        {
            var result = new List<GeoCoordinate>();
            var wrappedList = zeroSpeedPositions.Select(_ => new Wrapper<GeoCoordinate>(_)).ToList();
            foreach (var current in wrappedList)
            {
                if (!current.IsDeleted)
                {
                    var duplicates = wrappedList
                        .Where(_ => !_.IsDeleted && _.Coordinate.GetDistanceTo(current.Coordinate) < 50)
                        .ToList();

                    foreach (var item in duplicates)
                    {
                        item.IsDeleted = true;
                    }

                    var avgX = duplicates.Average(x => x.Coordinate.Latitude);
                    var avgY = duplicates.Average(x => x.Coordinate.Longitude);
                    result.Add(new GeoCoordinate(Math.Round(avgX, 6), Math.Round(avgY, 6)));
                }
            }

            return result;
        }

        private static Tuple<GeoCoordinate, GeoCoordinate> GetMostDistantPoints(IReadOnlyCollection<GeoCoordinate> positions)
        {
            var mostDistantPoints = new Dictionary<double, Tuple<GeoCoordinate, GeoCoordinate>>();
            foreach (var point in positions)
            {
                var mostDistantPoint = positions.OrderByDescending(_ => point.GetDistanceTo(_)).First();
                mostDistantPoints.TryAdd(point.GetDistanceTo(mostDistantPoint), new Tuple<GeoCoordinate, GeoCoordinate>(point, mostDistantPoint));
            }

            return mostDistantPoints[mostDistantPoints.Max(_ => _.Key)];
        }

        private static IEnumerable<DateTime> GetCountAtPoint(IEnumerable<RawDataModel> models, GeoCoordinate point)
        {
            return models.Where(_ => new GeoCoordinate(_.Latitude, _.Longitude).GetDistanceTo(point) < 50).Select(_ => _.TimeStamp);
        }

        private static IEnumerable<RawDataModel> RemoveTimeDuplicates(IEnumerable<RawDataModel> zeroSpeedPositions)
        {
            var result = new List<RawDataModel>();
            var wrappedList = zeroSpeedPositions.Select(_ => new Wrapper<RawDataModel>(_)).ToList();
            foreach (var current in wrappedList)
            {
                if (!current.IsDeleted)
                {
                    var duplicates = wrappedList.SkipWhile(_ => _ != current).Skip(1)
                        .TakeWhile(_ => !_.IsDeleted 
                            && new GeoCoordinate(_.Coordinate.Latitude, _.Coordinate.Longitude).GetDistanceTo(new GeoCoordinate(current.Coordinate.Latitude, current.Coordinate.Longitude)) < 50)
                        .ToList();

                    foreach (var item in duplicates)
                    {
                        item.IsDeleted = true;
                    }

                    result.Add(current.Coordinate);
                }
            }

            return result;
        }

        private static IEnumerable<GeoCoordinate> RemoveTimeDuplicates(IEnumerable<GeoCoordinate> zeroSpeedPositions)
        {
            var result = new List<GeoCoordinate>();
            var wrappedList = zeroSpeedPositions.Select(_ => new Wrapper<GeoCoordinate>(_)).ToList();
            foreach (var current in wrappedList)
            {
                if (!current.IsDeleted)
                {
                    var duplicates = wrappedList.SkipWhile(_ => _ != current).Skip(1)
                        .TakeWhile(_ => !_.IsDeleted && _.Coordinate.GetDistanceTo(current.Coordinate) < 50)
                        .ToList();

                    foreach (var item in duplicates)
                    {
                        item.IsDeleted = true;
                    }

                    result.Add(current.Coordinate);
                }
            }

            return result;
        }

        private static GeoCoordinate GetMostVisitedPoint(IEnumerable<GeoCoordinate> zeroSpeedPositions)
        {
            var result = RemoveTimeDuplicates(zeroSpeedPositions).ToList();
            var groupedByFrequency = AnalyzeFrequency(result, result);
            var max = groupedByFrequency.Max(_ => _.Value);
            var point = groupedByFrequency.First(_ => _.Value == max).Key;
            return point;
        }
    }
}