﻿namespace BusTracker.Analyzer.Console
{
    using System;
    using System.Diagnostics;

    [Serializable]
    [DebuggerDisplay("Time={TimeStamp.ToString(\"HH:mm:ss\")}, [{Latitude}, {Longitude}], Speed={Speed}, Satellite={Satellite}")]
    public class RawDataModel
    {
        public RawDataModel()
        {}

        public RawDataModel(string imei, DateTime timeStamp, double latitude, double longitude, byte speed, byte satellite)
        {
            this.Imei = imei;
            this.TimeStamp = timeStamp;
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.Speed = speed;
            this.Satellite = satellite;
        }

        public string Imei { get;}

        public DateTime TimeStamp { get;}

        public double Latitude { get;}

        public double Longitude { get;}

        public byte Speed { get;}

        public byte Satellite { get;}
    }
}