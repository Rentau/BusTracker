﻿namespace BusTracker.Analyzer.Console
{
    using System.Diagnostics;
    using System.IO;
    using BusTracker.Analyzer.Console.Repositories;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Serilog;


    class Program
    {
        static void Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("DatabaseSettings.json", false, true)
                .AddJsonFile("LoggerSettings.json", false, true);

            if (Debugger.IsAttached)
            {
                configBuilder.AddJsonFile("DatabaseSettings.Development.json", optional: true, true);
                configBuilder.AddJsonFile("LoggerSettings.Development.json", optional: true, true);
            }

            var config = configBuilder.Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();

            var serviceProvider = new ServiceCollection()
                .AddLogging(loggingBuilder =>
                {
                    loggingBuilder.AddSerilog();
                })
                .AddSingleton<IConfiguration>(config)
                .AddSingleton<Core, Core>()
                //.AddSingleton<ITrackDataRepository, RawTrackDataRepository>()
                .AddSingleton<ITrackDataRepository, TestTrackDataRepository>()
                .BuildServiceProvider();

            var core = serviceProvider.GetService<Core>();
            core.Analyze();
            Log.CloseAndFlush();
        }
    }
}
