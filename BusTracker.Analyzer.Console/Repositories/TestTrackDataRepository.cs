﻿namespace BusTracker.Analyzer.Console.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;

    class TestTrackDataRepository : ITrackDataRepository
    {
        private readonly List<RawDataModel> cache;

        public TestTrackDataRepository()
        {
            this.cache = InitializeCache();
        }

        public IEnumerable<RawDataModel> GetTrackDataByDate(DateTime date)
        {
            return this.cache.Where(_ => _.TimeStamp.Date == date.Date);
        }

        public IEnumerable<RawDataModel> GetTrackDataByImei(string imei)
        {
            return this.cache.Where(_ => _.Imei == imei);
        }

        private List<RawDataModel> InitializeCache()
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream("TestData/TestTracks.dat", FileMode.Open))
            {
                using (var zipStream = new GZipStream(fs, CompressionMode.Decompress))
                {
                    return formatter.Deserialize(zipStream) as List<RawDataModel>;
                }
            }
        }
    }
}
