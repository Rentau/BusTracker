﻿namespace BusTracker.Analyzer.Console
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using BusTracker.Analyzer.Console.Repositories;
    using Dapper;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;

    public class RawTrackDataRepository : ITrackDataRepository
    {
        private readonly IConfiguration config;
        private readonly ILogger<RawTrackDataRepository> logger;

        public RawTrackDataRepository(IConfiguration config, ILogger<RawTrackDataRepository> logger)
        {
            this.config = config;
            this.logger = logger;
        }

        public IDbConnection Connection => new SqlConnection(this.config.GetConnectionString("TrackLogsDatabase"));

        public IEnumerable<RawDataModel> GetTrackDataByDate(DateTime date)
        {
            var rawData = Enumerable.Empty<RawDataModel>();
            const string query = "SELECT [imei] Imei, [time] TimeStamp, [lat] Longitude, [lng] Latitude, [sat] Satellite, [spd] Speed FROM [TrackLogsDatabase].[tracking].[dozortracklog] WHERE CONVERT(DATE, time) = CONVERT(DATE, @date)";

            using (var connection = this.Connection)
            {
                connection.Open();

                try
                {
                    rawData = connection.Query<RawDataModel>(query, new { date = date.Date }).ToList();
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, "Can't insert Dozor tracks data into TrackLogsDatabase");
                }
            }

            return rawData;
        }

        public IEnumerable<RawDataModel> GetTrackDataByImei(string imei)
        {
            var rawData = Enumerable.Empty<RawDataModel>();
            const string query = "SELECT [imei] Imei, [time] TimeStamp, [lat] Longitude, [lng] Latitude, [sat] Satellite, [spd] Speed FROM [TrackLogsDatabase].[tracking].[dozortracklog] WHERE imei = @trackerId";

            using (var connection = this.Connection)
            {
                connection.Open();

                try
                {
                    rawData = connection.Query<RawDataModel>(query, new { trackerId = imei }).ToList();
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex, "Can't insert Dozor tracks data into TrackLogsDatabase");
                }
            }

            return rawData;
        }
    }
}