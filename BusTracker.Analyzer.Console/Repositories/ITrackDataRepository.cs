﻿using System;
using System.Collections.Generic;

namespace BusTracker.Analyzer.Console.Repositories
{
    public interface ITrackDataRepository
    {
        IEnumerable<RawDataModel> GetTrackDataByDate(DateTime date);

        IEnumerable<RawDataModel> GetTrackDataByImei(string imei);
    }
}