﻿namespace BusTracker.Analyzer.Console
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GeoCoordinatePortable;
    using GeoJSON.Net.Feature;
    using GeoJSON.Net.Geometry;

    public class GeoJsonHelper
    {
        public static Feature CreateFeature(IEnumerable<GeoCoordinate> coordinates, IDictionary<string, object> properties)
        {
            var jsonPoint = coordinates.Select(_ => new Point(new Position(_.Latitude, _.Longitude)));
            var multiPoint = new MultiPoint(jsonPoint);
            return new Feature(multiPoint, properties);
        }

        public static FeatureCollection GetFeatures(Dictionary<GeoCoordinate, int> points)
        {
            var avg = points.Average(_ => _.Value);
            var standardDeviation = Math.Sqrt(points.Average(_ => Math.Pow(_.Value - avg, 2)));

            var data = points.ToLookup(_ => Math.Abs(_.Value - avg) > 3 * standardDeviation);
            var max = data[false].Max(_ => _.Value);

            IDictionary<string, object> outlier = new Dictionary<string, object>
            {
                {"marker-color", "#000000"},
                {"marker-size", "small"},
                {"marker-symbol", "circle-stroked"},
                {"name", "Outlier"},
                {"formula", $"ABS(X - {avg:####.##}) > {2 * standardDeviation:####.##}"},
            };

            IDictionary<string, object> veryLowFrequency = new Dictionary<string, object>
            {
                {"marker-color", "#ff0000"},
                {"marker-size", "small"},
                {"marker-symbol", "circle-stroked"},
                {"name", "Very Low Frequency"},
                {"from", $"{max/5*0}"},
                {"to", $"{max/5*1}"},
            };

            IDictionary<string, object> lowFrequency = new Dictionary<string, object>
            {
                {"marker-color", "#ff9966"},
                {"marker-size", "small"},
                {"marker-symbol", "circle"},
                {"name", "Low Frequency"},
                {"from", $"{max/5*1}"},
                {"to", $"{max/5*2}"}, 
            };

            IDictionary<string, object> mediumFrequency = new Dictionary<string, object>
            {
                {"marker-color", "#80ff80"},
                {"marker-size", "small"},
                {"marker-symbol", "circle"},
                {"name", "Medium Frequency"},
                {"from", $"{max/5*2}"},
                {"to", $"{max/5*3}"},
            };

            IDictionary<string, object> highFrequency = new Dictionary<string, object>
            {
                {"marker-color", "#66ff99"},
                {"marker-size", "small"},
                {"marker-symbol", "circle"},
                {"name", "High Frequency"},
                {"from", $"{max/5*3}"},
                {"to", $"{max/5*4}"},
            };

            IDictionary<string, object> veryHighFrequency = new Dictionary<string, object>
            {
                {"marker-color", "#00cc00"},
                {"marker-size", "small"},
                {"marker-symbol", "circle-stroked"},
                {"name", "Very High Frequency"},
                {"from", $"{max/5*4}"},
                {"to", $"{max/5*5}"},
            };

            var validPoints = data[false].ToList();
            var outlierPoints = data[true].ToList();
            var features = new List<Feature>
            {
                CreateFeature(outlierPoints.Select(_ => _.Key), outlier),
                CreateFeature(validPoints.Where(_ => _.Value <= max / 5 * 1).Select(_ => _.Key), veryLowFrequency),
                CreateFeature(validPoints.Where(_ => max / 5 * 1 < _.Value && _.Value <= max / 5 * 2).Select(_ => _.Key), lowFrequency),
                CreateFeature(validPoints.Where(_ => max / 5 * 2 < _.Value && _.Value <= max / 5 * 3).Select(_ => _.Key), mediumFrequency),
                CreateFeature(validPoints.Where(_ => max / 5 * 3 < _.Value && _.Value <= max / 5 * 4).Select(_ => _.Key), highFrequency),
                CreateFeature(validPoints.Where(_ => max / 5 * 4 < _.Value).Select(_ => _.Key), veryHighFrequency),
            };
            
            return new FeatureCollection(features);
        }

        internal static FeatureCollection CreateFeatures(List<Core.StopInfo> allRoutesEndPoint)
        {
            var features = new List<Feature>();

            foreach (var endPoint in allRoutesEndPoint)
            {
                var imei = endPoint.Point.Imei;
                long number = long.Parse(imei);
                var hexString = number.ToString("X");
                var color = hexString.Substring(hexString.Length - 6);

                IDictionary<string, object> style = new Dictionary<string, object>
                {
                    {"marker-color", $"#{color}"},
                    {"marker-size", "small"},
                    {"marker-symbol", "circle-stroked"},
                    {"name", $"{imei}"},
                    {"date", $"{endPoint.Point.TimeStamp:g}"},
                    {"from", $"{endPoint.FromTime:g}"},
                    {"to", $"{endPoint.ToTime:g}"},
                    {"count", $"{endPoint.Count}" }
                };

                features.Add(
                    CreateFeature(new [] { new GeoCoordinate(endPoint.Point.Latitude, endPoint.Point.Longitude)}, style));

            }

            return new FeatureCollection(features);
        }
    }
}