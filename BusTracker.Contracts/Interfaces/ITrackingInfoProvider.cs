﻿namespace BusTracker.Contracts.Interfaces
{
    using System.Collections.Generic;

    public interface ITrackingInfoProvider
    {
        IEnumerable<IVehicleTracker> GetTrackingUpdates();
    }
}
