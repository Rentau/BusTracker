﻿namespace BusTracker.Contracts.Interfaces
{
    using System;

    public interface IVehicleTracker
    {
        DateTime TimeStamp { get; }

        byte Speed { get; }

        byte Satellite { get; }

        double Longitude { get; }

        double Latitude { get; }

        string Imei { get; }
    }
}