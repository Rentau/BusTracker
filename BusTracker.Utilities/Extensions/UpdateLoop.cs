﻿namespace BusTracker.Utilities.Extensions
{
    using System;
    using System.Threading;

    public static class UpdateLoop
    {
        public static void RunUntilEscape(Action action, int threshold)
        {
            do
            {
                while (!Console.KeyAvailable)
                {
                    action?.Invoke();
                    Thread.Sleep(threshold);
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}