﻿namespace BusTracker.Infrastructure.CQRS.Commands
{
    using System;
    using BusTracker.Contracts.Interfaces;
    using NetTopologySuite.Geometries;

    public class AddTrackingDataCommand : ICommand
    {
        public AddTrackingDataCommand(IVehicleTracker trackerInformation)
        {
            this.TimeStamp = trackerInformation.TimeStamp;
            this.Speed = trackerInformation.Speed;
            this.Location = new Point(trackerInformation.Latitude, trackerInformation.Longitude)
            {
                SRID = 4326
            };
            this.Imei = trackerInformation.Imei;
        }
        
        public DateTime TimeStamp { get; }

        public byte Speed { get; }

        public Point Location { get; }

        public string Imei { get; }
    }
}