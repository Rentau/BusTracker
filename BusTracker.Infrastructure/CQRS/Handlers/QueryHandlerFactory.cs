﻿namespace BusTracker.Infrastructure.CQRS.Handlers
{
    using System;
    using Microsoft.Extensions.DependencyInjection;

    public class QueryHandlerFactory : IQueryHandlerFactory
    {
        private readonly IServiceProvider serviceProvider;

        public QueryHandlerFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public TQueryHandler Resolve<TQueryHandler>()
        {
            return this.serviceProvider.GetService<TQueryHandler>();
        }
    }
}