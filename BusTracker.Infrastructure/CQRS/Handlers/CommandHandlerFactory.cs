﻿namespace BusTracker.Infrastructure.CQRS.Handlers
{
    using System;
    using Microsoft.Extensions.DependencyInjection;

    public class CommandHandlerFactory : ICommandHandlerFactory
    {
        public IServiceProvider ServiceProvider { get; set; }

        public CommandHandlerFactory(IServiceProvider serviceProvider)
        {
            this.ServiceProvider = serviceProvider;
        }

        public TCommandHandler Resolve<TCommandHandler>()
        {
            return this.ServiceProvider.GetService<TCommandHandler>();
        }
    }
}