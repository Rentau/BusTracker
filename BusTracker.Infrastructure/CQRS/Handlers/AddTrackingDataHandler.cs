﻿namespace BusTracker.Infrastructure.CQRS.Handlers
{
    using System;
    using System.Threading.Tasks;
    using BusTracker.Infrastructure.CQRS.Commands;
    using BusTracker.Infrastructure.Data.Contexts;
    using BusTracker.Infrastructure.Data.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    public class AddTrackingDataHandler : ICommandHandler<AddTrackingDataCommand>
    {
        private readonly IReadWriteContext context;
        private readonly ILogger<AddTrackingDataHandler> logger;

        public AddTrackingDataHandler(IReadWriteContext context, ILogger<AddTrackingDataHandler> logger)
        {
            this.context = context;
            this.logger = logger;
        }

        public async Task Execute(AddTrackingDataCommand command)
        {
            try
            {
                var existingBusStation = await this.context.TrackingDataLog.FirstOrDefaultAsync(_ => _.DeviceName == command.Imei && _.Time == command.TimeStamp);

                if (existingBusStation == null)
                {
                    var trackingData = new TrackingDataLog()
                    {
                        DeviceName = command.Imei,
                        Time = command.TimeStamp,
                        Location = command.Location,
                        Speed = command.Speed 
                    };

                    await this.context.TrackingDataLog.AddAsync(trackingData);
                }
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e.GetBaseException().Message);
                throw;
            }

        }
    }
}