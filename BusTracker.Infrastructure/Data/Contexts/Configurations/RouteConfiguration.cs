﻿namespace BusTracker.Infrastructure.Data.Contexts.Configurations
{
    using BusTracker.Infrastructure.Data.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class RouteConfiguration : EntityConfiguration<Route>
    {
        public override void Configure(EntityTypeBuilder<Route> builder)
        {
            builder.HasOne(_ => _.FirstStationRouteStationMapping)
                .WithMany(_ => _.FirstStationForRoutes)
                .HasForeignKey(_ => _.FirstStationMappingId);

            builder.HasOne(_ => _.LastStationsRouteStationMapping)
                .WithMany(_ => _.LastStationForRoutes)
                .HasForeignKey(_ => _.LastStationMappingId);

            builder.HasMany(_ => _.Vehicles)
                .WithOne(_ => _.Route)
                .HasForeignKey(_ => _.Id);

            base.Configure(builder);
        }
    }
}