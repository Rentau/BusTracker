﻿namespace BusTracker.Infrastructure.Data.Contexts
{
    using Microsoft.EntityFrameworkCore;

    public sealed class ReadOnlyBusTrackerContext : ReadWriteBusTrackerContext, IReadOnlyContext
    {
        public ReadOnlyBusTrackerContext(DbContextOptions options) : base(options)
        {
            this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
    }
}