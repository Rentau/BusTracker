﻿namespace BusTracker.Infrastructure.Data.Entities
{
    using System.Collections.Generic;

    public class TrackingDevice : Entity
    {
        public string Imei { get; set; }

        public Vehicle Vehicle { get; set; }

        public ICollection<TrackingDataLog> TrackingData { get; set; }
    }
}