﻿namespace BusTracker.Infrastructure.EventHandling
{
    using System;
    using BusTracker.Contracts.Interfaces;

    public interface IEventAggregator
    {
        void Publish<TEvent>(TEvent message) where TEvent : IEvent;
        Subscription<TEvent> Subscribe<TEvent>(Action<TEvent> action) where TEvent : IEvent;
        void UnSubscribe<TEvent>(Subscription<TEvent> subscription) where TEvent : IEvent;
    }
}