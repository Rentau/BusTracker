﻿namespace BusTracker.Infrastructure.EventHandling
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using BusTracker.Contracts.Interfaces;

    public class EventAggregator : IEventAggregator
    {
        private readonly Dictionary<Type, IList> subscribers;

        public EventAggregator()
        {
            this.subscribers = new Dictionary<Type, IList>();
        }

        public void Publish<TEvent>(TEvent message) where TEvent : IEvent
        {
            var t = typeof(TEvent);
            if (this.subscribers.ContainsKey(t))
            {
                var subscriptions = new List<Subscription<TEvent>>(this.subscribers[t].Cast<Subscription<TEvent>>());

                foreach (var subscription in subscriptions)
                {
                    subscription.Action(message);
                }
            }
            else
            {
                Debug.WriteLine($"No subscribers found for {t.Name} message type");
            }
        }

        public Subscription<TEvent> Subscribe<TEvent>(Action<TEvent> action) where TEvent : IEvent
        {
            var t = typeof(TEvent);
            var subscription = new Subscription<TEvent>(action, this);

            if (!this.subscribers.TryGetValue(t, out var subscriptions))
            {
                subscriptions = new List<Subscription<TEvent>> { subscription };
                this.subscribers.Add(t, subscriptions);
            }
            else
            {
                subscriptions.Add(subscription);
            }

            return subscription;
        }

        public void UnSubscribe<TEvent>(Subscription<TEvent> subscription) where TEvent : IEvent
        {
            var t = typeof(TEvent);
            if (this.subscribers.ContainsKey(t))
            {
                this.subscribers[t].Remove(subscription);
            }
        }
    }
}
