﻿namespace BusTracker.Infrastructure.EventHandling
{
    using System;
    using BusTracker.Contracts.Interfaces;

    public class Subscription<TEvent> : IDisposable where TEvent : IEvent
    {
        public Action<TEvent> Action { get; }

        private readonly EventAggregator eventAggregator;

        public Subscription(Action<TEvent> action, EventAggregator eventAggregator)
        {
            this.Action = action;
            this.eventAggregator = eventAggregator;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.eventAggregator.UnSubscribe(this);
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~Subscription()
        {
            this.Dispose(false);
        }
    }
}