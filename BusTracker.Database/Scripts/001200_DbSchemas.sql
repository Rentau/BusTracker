IF NOT EXISTS (
		SELECT 1
		FROM [sys].[schemas] s
		WHERE s.[name] = 'tracker'
		)
BEGIN
	PRINT 'Creating Schema Tracker'

	EXEC ('CREATE SCHEMA tracker')
END