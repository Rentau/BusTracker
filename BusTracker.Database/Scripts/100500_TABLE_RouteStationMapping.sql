IF NOT EXISTS (
	SELECT 1
	FROM [sys].[objects] o
	WHERE o.[object_id] = OBJECT_ID(N'[tracker].[routestationmaping]')
		AND o.[type] = N'U'
	)
BEGIN

PRINT 'Creating Table [tracker].[routestationmaping]'

CREATE TABLE [tracker].[routestationmaping](
	 [id]								INT IDENTITY (1, 1) NOT NULL
	,[date_created]			DATETIME2 (2)    DEFAULT (SYSUTCDATETIME()) NOT NULL
	,[date_updated]			DATETIME2 (2)    NULL
	,[route_id]							INT NOT NULL
	,[station_id]						INT NOT NULL
	,[previous_station_route_mapping]	INT NOT NULL
	,[next_station_route_mapping]		INT NOT NULL
	,CONSTRAINT [pk_routestationmaping] PRIMARY KEY CLUSTERED ([id] ASC)
)

END

GO