IF NOT EXISTS (
	SELECT 1
	FROM [sys].[objects] o
	WHERE o.[object_id] = OBJECT_ID(N'[tracker].[trackingdevice]')
		AND o.[type] = N'U'
	)
BEGIN

PRINT 'Creating Table [tracker].[trackingdevice]'

CREATE TABLE [tracker].[trackingdevice](
	 [id]					INT IDENTITY (1, 1) NOT NULL
	,[date_created]			DATETIME2 (2)    DEFAULT (SYSUTCDATETIME()) NOT NULL
	,[date_updated]			DATETIME2 (2)    NULL
	,[imei]					BIGINT NOT NULL
	,CONSTRAINT [pk_trackingdevice] PRIMARY KEY CLUSTERED ([id] ASC)
)

END

GO