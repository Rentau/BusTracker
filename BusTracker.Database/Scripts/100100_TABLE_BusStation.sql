IF NOT EXISTS (
	SELECT 1
	FROM [sys].[objects] o
	WHERE o.[object_id] = OBJECT_ID(N'[tracker].[busstation]')
		AND o.[type] = N'U'
	)
BEGIN

PRINT 'Creating Table [tracker].[busstation]'

CREATE TABLE [tracker].[busstation](
	 [id]					INT IDENTITY (1, 1) NOT NULL
	,[date_created]			DATETIME2 (2)    DEFAULT (SYSUTCDATETIME()) NOT NULL
	,[date_updated]			DATETIME2 (2)    NULL
	,[internal_place_id]	NVARCHAR (300) NOT NULL
	,[name]					NVARCHAR (300) NOT NULL
	,[location]				GEOMETRY NOT NULL
	,CONSTRAINT [pk_busstation] PRIMARY KEY CLUSTERED ([id] ASC)
)

END

GO