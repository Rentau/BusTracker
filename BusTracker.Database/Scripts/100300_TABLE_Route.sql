IF NOT EXISTS (
	SELECT 1
	FROM [sys].[objects] o
	WHERE o.[object_id] = OBJECT_ID(N'[tracker].[route]')
		AND o.[type] = N'U'
	)
BEGIN

PRINT 'Creating Table [tracker].[route]'

CREATE TABLE [tracker].[route](
	 [id]					INT IDENTITY (1, 1) NOT NULL
	,[date_created]			DATETIME2 (2)    DEFAULT (SYSUTCDATETIME()) NOT NULL
	,[date_updated]			DATETIME2 (2)    NULL
	,[name]	INT NOT NULL
	,[first_station_id]		INT NOT NULL
	,[last_station_id]		INT NOT NULL
	,CONSTRAINT [pk_route] PRIMARY KEY CLUSTERED ([id] ASC)
)

END

GO