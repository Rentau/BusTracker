IF NOT EXISTS (
	SELECT 1
	FROM [sys].[objects] o
	WHERE o.[object_id] = OBJECT_ID(N'[tracker].[trackingdatalog]')
		AND o.[type] = N'U'
	)
BEGIN

PRINT 'Creating Table [tracker].[trackingdatalog]'

CREATE TABLE [tracker].[trackingdatalog](
	 [id]					INT IDENTITY (1, 1) NOT NULL
	,[date_created]			DATETIME2 (2)		DEFAULT (SYSUTCDATETIME()) NOT NULL
	,[date_updated]			DATETIME2 (2)		NULL
	,[device_name]			VARCHAR (30)		NOT NULL
	,[location]				GEOMETRY			NOT NULL
	,[speed]				TINYINT				NOT NULL
	,[date_tracked]			DATETIME2			NOT NULL
	,CONSTRAINT [pk_trackingdatalog] PRIMARY KEY CLUSTERED ([id] ASC)
)

END

GO