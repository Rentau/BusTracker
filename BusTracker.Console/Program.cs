﻿namespace BusTracker.Console
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;
    using BusTracker.Core;
    using BusTracker.Infrastructure.CQRS.Commands;
    using BusTracker.Infrastructure.CQRS.Dispatchers;
    using BusTracker.Infrastructure.CQRS.Handlers;
    using BusTracker.Infrastructure.Data.Contexts;
    using BusTracker.Infrastructure.EventHandling;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Serilog;

    internal static class Program
    {
        private static void Main()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("DatabaseSettings.json", false, true)
                .AddJsonFile("LoggerSettings.json", false, true);

            if (Debugger.IsAttached)
            {
                configBuilder.AddJsonFile("DatabaseSettings.Development.json", optional: true, true);
                configBuilder.AddJsonFile("LoggerSettings.Development.json", optional: true, true);
            }

            var config = configBuilder.Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();

            var serviceProvider = new ServiceCollection()
                .AddLogging(loggingBuilder =>
                {
                    loggingBuilder.AddConsole();
                    loggingBuilder.AddSerilog();
                    loggingBuilder.AddDebug();
                })
                .AddSingleton<ICore, Core>()
                .AddSingleton<IEventAggregator, EventAggregator>()
                .AddSingleton<ICommandDispatcher, CommandDispatcher>()
                .AddDbContext<IReadOnlyContext, ReadOnlyBusTrackerContext>(
                    options => options.UseSqlServer(
                        config.GetConnectionString("DefaultConnection"),
                        x => x.UseNetTopologySuite()))
                .AddDbContext<IReadWriteContext, ReadWriteBusTrackerContext>(
                    options => options.UseSqlServer(
                        config.GetConnectionString("DefaultConnection"),
                        x => x.UseNetTopologySuite()))
                .AddSingleton<ICommandHandlerFactory, CommandHandlerFactory>()
                .AddSingleton<ICommandHandler<AddTrackingDataCommand>, AddTrackingDataHandler>()
                //.AddSingleton<IQueryDispatcher<>, QueryDispatcher<>>()
                .BuildServiceProvider();

            var logger = serviceProvider.GetService<ILoggerFactory>().CreateLogger(nameof(Program));
            logger.LogDebug("Starting application");

            var core = serviceProvider.GetService<ICore>();

            logger.LogDebug("All done!");
        }

        private static void RunUntilEscape(Action action, int threshold)
        {
            do
            {
                while (!Console.KeyAvailable)
                {
                    action?.Invoke();
                    Thread.Sleep(threshold);
                    //Console.Clear();
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
